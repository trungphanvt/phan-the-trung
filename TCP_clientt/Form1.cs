﻿using SimpleTcp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using MaterialSkin.Controls;

namespace TCP_clientt
{
    //hello
    //Hi trung
    //heeee
    public partial class Form1 : MaterialForm
    {
        public Form1()
        {
            InitializeComponent();
           
            CheckForIllegalCrossThreadCalls = false;
            
            Connect();
            Connect2();
            Connect3();
        }
        SimpleTcpClient client1;
      

        IPEndPoint IP; 
        Socket client;
        
        private void btnSend_Click_1(object sender, EventArgs e)
        {
            if (client1.IsConnected)
            {
                if (!string.IsNullOrEmpty(txtMessage.Text) && Int32.Parse(txtMessage.Text) > Int32.Parse(txtPrice.Text))
                {
                    client1.Send(txtMessage.Text);
                    txtPP.Text = txtMessage.Text;
                    txtInfo.Text += $"Me: {txtMessage.Text}{Environment.NewLine}";
                    txtMessage.Text = string.Empty;
                    
                    
                }
                else
                {
                    MessageBox.Show("Nhap lai");
                }
            }
        }
        private void btnConnect_Click_1(object sender, EventArgs e)
        {
            try
            {
                client1.Connect();
                btnSend.Enabled = true;
                btnConnect.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            client1 = new(txtIP.Text);
            client1.Events.Connected += Events_Connected;
            client1.Events.DataReceived += Events_DataReceived;
            client1.Events.Disconnected += Events_Disconnected;

            btnSend.Enabled = false;
        }

        private void Events_Disconnected(object sender, ClientDisconnectedEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                txtInfo.Text += $"Server disconnected.{Environment.NewLine}";
            });


        }

        private void Events_DataReceived(object sender, DataReceivedEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                //byte[] B = e.Data;
                //picture.Image = ReturnImgae(B);
                //Bitmap bmp = new Bitmap(picture.Image);
                txtInfo.Text += $"Server: {Encoding.UTF8.GetString(e.Data)}{Environment.NewLine}";
                //picture.Image = bmp;
            });

        }

        private void Events_Connected(object sender, ClientConnectedEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                txtInfo.Text += $"Server connected.{Environment.NewLine}";
            });
        }
        
        /// Demo
        void Connect()
        {
            IP = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9999);
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream,ProtocolType.IP);

            try
            {
                client.Connect(IP);
                
            }
            catch
            {
                MessageBox.Show("Khong the ket noi duoc !", "Loi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            Thread listen = new Thread(Recevie);
            listen.IsBackground = true;
            listen.Start();

            timer2.Start();
          
        }
        


        void Close()
        {
            client.Close();
        }

        void Send()
        {
            if (txtMessage.Text != string.Empty) ;
            client.Send(Serialize(txtMessage.Text));
        }

        void Recevie()
        {
            try
            {
                while (true)
                {
                    byte[] data = new byte[1024 * 5000];
                    client.Receive(data);
                    string message = (string)Deserialize(data);                    
                    AddMess(message);
                    
                }
            }
            catch
            {
                Close();
            }
            
        }
         void AddMess(string s)
        {

            txtThongtin.Text = s;
            
            txtMessage.Clear();
        }
        byte[] Serialize(object obj)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();

            formatter.Serialize(stream,obj);
            return stream.ToArray();
        }
        object Deserialize(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryFormatter formatter = new BinaryFormatter();

            return formatter.Deserialize(stream);
            
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Close();
            Close2();
            Close3();
        }

        private void btnPrice_Click(object sender, EventArgs e)
        {
            
            AddMess(txtMessage.Text);
        }

        // timer
        int d = 0;
        private void timer2_Tick(object sender, EventArgs e)
        {
           
            if (txtThongtin.Text == "Start")
            {
                timer1.Start();
            }
            
            if (d==20)
            {
                timer2.Stop();
                timer1.Stop();
                lbMi.Text = "00";
                lbS.Text = "00";
                
                if (Int32.Parse(txtPP.Text)==Int32.Parse(txtPrice.Text))
                {
                    MessageBox.Show(" Chuc mừng bạn đấu giá thành công");
                }
                else { MessageBox.Show("Ket thuc"); } 
            }    


        }
        int i = 20;
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            i--;
            lbMi.Text = "00";
            lbS.Text = i.ToString();
            d++;
           

        }
        // capnhatgia
        IPEndPoint IP2;
        Socket client2;
        void Connect2()
        {
            IP2 = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8888);
            client2 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

            try
            {
                client2.Connect(IP2);

            }
            catch
            {
                MessageBox.Show("Khong the ket noi duoc 2!", "Loi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            Thread listen2 = new Thread(Recevie2);
            listen2.IsBackground = true;
            listen2.Start();
        }

        void Recevie2()
        {
            try
            {
                while (true)
                {

                    byte[] data2 = new byte[1024 * 5000];
                    client2.Receive(data2);
                    string message2 = (string)Deserialize(data2);
                    txtPrice.Text = message2;
                    

                }
            }

            catch
            {
                Close();
            }

        }
        //nhận ảnh
        IPEndPoint IP3;
        Socket client3;
        void Connect3()
        {
            IP3 = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 7777);
            client3 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

            try
            {
                client3.Connect(IP3);
                
            }
            catch
            {
                MessageBox.Show("Khong the ket noi duoc !", "Loi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            Thread listen3 = new Thread(Recevie3);
            listen3.IsBackground = true;
            listen3.Start();
        }

        void Recevie3()
        {
            try
            {
                while (true)
                {
                    
                    byte[] data3 = new byte[1024 * 10000];
                    client3.Receive(data3);
                    MemoryStream stream = new MemoryStream(data3);
                    Bitmap bitmap = new Bitmap(stream);
                    picture.Image = bitmap;
                }
            }

            catch
            {
                Close();
            }

        }

        

        public Image ReturnImgae(byte[] bytes)
        {

            MemoryStream ms = new MemoryStream(bytes);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        void Close2()
        {
            client2.Close();
        }
        void Close3()
        {
            client3.Close();
        }
    }  
}
