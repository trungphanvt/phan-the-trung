using SimpleTcp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class Form1 : Form
    {
        IList<string> Clients = new List<string>() { };
        int h = 0;
        public Form1()
        {
            InitializeComponent();

        }

        SimpleTcpServer server;

        private void Form1_Load(object sender, EventArgs e)
        {
            server = new SimpleTcpServer(txtIP.Text);
        }


        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            server.Start();
            server.Events.ClientConnected += Events_ClientConnected;
            server.Events.ClientDisconnected += Events_ClientDisconnected;
            server.Events.DataReceived += Events_DataReceived;

            btnStart.Enabled = false;
        }

        private void Events_DataReceived(object sender, DataReceivedEventArgs e)
        {
            if (h <= Clients.Count + 1)
            {
                for (int i = 0; i < Clients.Count; i++)
                {
                    server.Send(Clients[i], $"{e.IpPort}: {Encoding.UTF8.GetString(e.Data)} {Environment.NewLine}");
                }


            }
            else
            {

                h = 0;
            }

            //string l = server.GetClients().ToString();
            //server.Send(l, $"{e.IpPort}: {Encoding.UTF8.GetString(e.Data)} {Environment.NewLine}");
        }

        private void Events_ClientDisconnected(object sender, ClientDisconnectedEventArgs e)
        {

        }

        private void Events_ClientConnected(object sender, ClientConnectedEventArgs e)
        {
            //lstClients.Items.Add(e.IpPort);
            Clients.Add(e.IpPort);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(694, 406);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.ResumeLayout(false);

        }

        private void Form1_Load_1(object sender, EventArgs e)
        {

        }
    }
}