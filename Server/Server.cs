﻿using SimpleTcp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class Server : Form
    {
        IList<string> Clients = new List<string>() { };
        int h = 0;
        public Server()
        {
            InitializeComponent();
        }
        SimpleTcpServer server;
       
        private void Server_Load(object sender, EventArgs e)
        {
          
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            server.Start();
            server.Events.ClientConnected += Events_ClientConnected;
            server.Events.ClientDisconnected += Events_ClientDisconnected;
            server.Events.DataReceived += Events_DataReceived;
            btnStart.Enabled = false;

        }

        private void Events_DataReceived(object sender, DataReceivedEventArgs e)
        {
            if (h <= Clients.Count + 1)
            {
                for (int i = 0; i < Clients.Count; i++)
                {
                    server.Send(Clients[i], $"{e.IpPort}: {Encoding.UTF8.GetString(e.Data)} {Environment.NewLine}");
                }


            }
            else
            {

                h = 0;
            }
        }

        private void Events_ClientDisconnected(object sender, ClientDisconnectedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Events_ClientConnected(object sender, ClientConnectedEventArgs e)
        {
            Clients.Add(e.IpPort);
        }
        
    }
}
