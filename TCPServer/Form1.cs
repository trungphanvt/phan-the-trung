﻿using SimpleTcp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin.Controls;

namespace TCPServer
{
    public partial class Form1 : MaterialForm
    {

        public Form1()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            Connect();//Server dùng để Gửi thông tin đồng loạt vs tất cả các Clienrt
            
            Connect2();//Server dùng để cập nhập liên túc giá cao nhất 
            Connect3();//Server dùng để gửi tất cả ảnh 
            timer2.Start();//Để cập nhật giá liên tục
            timer3.Start();
        }
        IPEndPoint IP;
        Socket Server;
        List<Socket> clientList;
        SimpleTcpServer server;
        
       
        private void btnStart_Click(object sender, EventArgs e)
        {
           
            server.Start();
            txtInfo.Text += $"Starting ...{Environment.NewLine}";
            btnStart.Enabled = false;
            btnSend.Enabled = true;
           
        }
       
        private void Form1_Load(object sender, EventArgs e)
        {
            btnSend.Enabled = false;
            server = new SimpleTcpServer(txtIP.Text);
            server.Events.ClientConnected += Events_ClientConnected;
            server.Events.ClientDisconnected += Events_ClientDisconnected;
            server.Events.DataReceived += Events_DataReceived;
        }
        string Price;
        private void Events_DataReceived(object sender, DataReceivedEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                txtInfo.Text += $"{e.IpPort} :{Encoding.UTF8.GetString(e.Data)}{Environment.NewLine}";
                txtPrice2.Clear();
                txtPrice2.Text += Encoding.UTF8.GetString(e.Data);
                

            });


        }

        private void Events_ClientDisconnected(object sender, ClientDisconnectedEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                txtInfo.Text += $"{e.IpPort} disconnected.{Environment.NewLine}";
                lstClientIP.Items.Remove(e.IpPort);
            });

        }

        private void Events_ClientConnected(object sender, ClientConnectedEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
                {
                    txtInfo.Text += $"{e.IpPort} connected.{Environment.NewLine}";
                    lstClientIP.Items.Add(e.IpPort);
                });


        }
        private void btnSend_Click(object sender, EventArgs e)
        {
            if (server.IsListening)
            {
                if (!string.IsNullOrEmpty(txtMessage.Text) && lstClientIP.SelectedItem != null)//check message & select Client  ip form listbox
                {
                    server.Send(lstClientIP.SelectedItem.ToString(), txtMessage.Text);
                    txtInfo.Text += $"Server: {txtMessage.Text}{Environment.NewLine}";
                    txtMessage.Text += string.Empty;

                }
            }
        }
       
        
       

      
        int i = 20,d=0;
      

        private void timer1_Tick(object sender, EventArgs e)
        {
            i--;
            lbH.Text = "00";
            lbM.Text = i.ToString();
            d++;
            if(d==20)
            {
                lbM.Text = "00";
                lbH.Text = "00";
                timer1.Stop();

            }    
             

        }
        Bitmap Bitmap;
      
       

        
      
        private void addInfo_Click(object sender, EventArgs e)
        {
            foreach (Socket item in clientList)
            {
                Send(item);

            }
            txtMessage.Clear();
        }


        public byte[] ImageToByte2(Image img)
        {
          
            MemoryStream ms = new MemoryStream();
            img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            return ms.ToArray();
        }
        public Image ReturnImgae(byte[] bytes)
        {

            MemoryStream ms = new MemoryStream(bytes);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        //UDP
        
     
        /// Demo
        void Connect()
        {
            clientList = new List<Socket>();
            IP = new IPEndPoint(IPAddress.Any, 9999);
            Server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

            Server.Bind(IP);

            Thread Listen = new Thread(() =>
            {
                try
                {
                    while (true)
                    {
                        Server.Listen(100);
                        Socket client = Server.Accept();
                        clientList.Add(client);

                        Thread receive = new Thread(Recevie);
                        receive.IsBackground = true;
                        receive.Start(client);

                    }
                }
                catch
                {
                    IP = new IPEndPoint(IPAddress.Any, 9999);
                    Server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                }
               
                
            });
            Listen.IsBackground = true;
            Listen.Start();
        }

        void Close()
        {
            Server.Close();

        }
        
        void Send(Socket client)
        {
            if (txtInfo.Text != string.Empty) 
            client.Send(Serialize(txtSendInfo.Text));
            txtThongtin.Text = txtSendInfo.Text;
            if (txtSendInfo.Text=="Start")
            {
                timer1.Start();
            }    

        }

       
        void Recevie(Object obj)
        {
            Socket client = obj as Socket;
            try
            {
                while (true)
                {
                    byte[] data = new byte[1024 * 5000];
                    client.Receive(data);
                    string message = (string)Deserialize(data);

                    AddMess(message);
                }
            }
            catch
            {
                clientList.Remove(client);
                client.Close();
            }

        }
        
            void AddMess(string s)
            {
                txtThongtin.Text=s;
                
        }
            byte[] Serialize(object obj)
            {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();

            formatter.Serialize(stream, obj);
            return stream.ToArray();
            }



            object Deserialize(byte[] data)
            {
                MemoryStream stream = new MemoryStream(data);
                BinaryFormatter formatter = new BinaryFormatter();

                return formatter.Deserialize(stream);

            }
        
        int dem = 0;
       
       
        private void timer2_Tick(object sender, EventArgs e)
        {
            int a = Int32.Parse(txtprice.Text);
            int b = Int32.Parse(txtPrice2.Text);
            if (b > a)
            {
                dem++;
                txtprice.Text = b.ToString();
                lbdem.Text = dem.ToString();
            }
        }

        // cập nhập giá
        IPEndPoint IP2;
        Socket Server2;
        List<Socket> clientList2;
        void Connect2()
        {
            clientList2 = new List<Socket>();
            IP2 = new IPEndPoint(IPAddress.Any, 8888);
            Server2 = new Socket(AddressFamily.InterNetwork, SocketType.Stream,ProtocolType.IP);

            Server2.Bind(IP2);

            Thread Listen2 = new Thread(() =>
            {
                try
                {
                    while (true)
                    {
                        Server2.Listen(100);
                        Socket client = Server2.Accept();
                        clientList2.Add(client);

                        Thread receive2 = new Thread(Recevie2);
                        receive2.IsBackground = true;
                        receive2.Start(client);
                     

                    }
                }
                catch
                {
                    IP2 = new IPEndPoint(IPAddress.Any, 8888);
                    Server2 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                }


            });
            
            Listen2.IsBackground = true;
            Listen2.Start();
          
        }
        void Close2()
        {
            Server2.Close();

        }
        void Recevie2(Object obj)
        {
            Socket client = obj as Socket;
            try
            {
                while (true)
                {
                    byte[] data = new byte[1024 * 5000];
                    client.Receive(data);
                    string message = (string)Deserialize(data);

                    
                }
            }
            catch
            {
                clientList.Remove(client);
                client.Close();
            }

        }
     
        void Send2(Socket client)
        {

            client.Send(Serialize(txtprice.Text));

        }

        private void timer3_Tick(object sender, EventArgs e)
        {
               
          if ( clientList2 != null)
            {
                foreach (Socket item in clientList2)
                {
                    Send2(item);
                }
            }
            

        }
        //gửi ảnh
        IPEndPoint IP3;
        Socket Server3;
        List<Socket> clientList3;
        void Connect3()
        {
            clientList3 = new List<Socket>();
            IP3 = new IPEndPoint(IPAddress.Any, 7777);
            Server3 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

            Server3.Bind(IP3);

            Thread Listen3 = new Thread(() =>
            {
                try
                {
                    while (true)
                    {
                        Server3.Listen(100);
                        Socket client = Server3.Accept();
                        clientList3.Add(client);

                        Thread receive3 = new Thread(Recevie3);
                        receive3.IsBackground = true;
                        receive3.Start(client);


                    }
                }
                catch
                {
                    IP3 = new IPEndPoint(IPAddress.Any, 7777);
                    Server3 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                }


            });

            Listen3.IsBackground = true;
            Listen3.Start();

        }
        void Close3()
        {
            Server3.Close();

        }
        void Recevie3(Object obj)
        {
            Socket client = obj as Socket;
            try
            {
                while (true)
                {
                    byte[] data = new byte[1024 * 5000];
                    client.Receive(data);
                    string message = (string)Deserialize(data);
                }
            }
            catch
            {
                clientList.Remove(client);
                client.Close();
            }

        }

        void Send3(Socket client)
        {

            client.Send(ImageToByteArray(pictureb.Image));

        }
        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }
      
        

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void addPicture_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Bitmap = new Bitmap(open.FileName);
                pictureb.Image = Bitmap;

            }
        }

       
        private void SendPicture_Click(object sender, EventArgs e)
        {
            foreach (Socket item in clientList3)
            {
                Send3(item);

            }
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

      

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Close();
            Close2();
            Close3();
        }

    }
}


