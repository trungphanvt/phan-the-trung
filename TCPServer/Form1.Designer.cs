﻿
namespace TCPServer
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtPrice2 = new System.Windows.Forms.TextBox();
            this.SendPicture = new MaterialSkin.Controls.MaterialRaisedButton();
            this.addPicture = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lbM = new System.Windows.Forms.Label();
            this.lbH = new System.Windows.Forms.Label();
            this.addInfo = new MaterialSkin.Controls.MaterialRaisedButton();
            this.label11 = new System.Windows.Forms.Label();
            this.btnSend = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnStart = new MaterialSkin.Controls.MaterialRaisedButton();
            this.txtThongtin = new System.Windows.Forms.TextBox();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSendInfo = new System.Windows.Forms.TextBox();
            this.lb = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbdem = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureb = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lstClientIP = new System.Windows.Forms.ListBox();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureb)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer3
            // 
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.txtPrice2);
            this.panel1.Controls.Add(this.SendPicture);
            this.panel1.Controls.Add(this.addPicture);
            this.panel1.Controls.Add(this.lbM);
            this.panel1.Controls.Add(this.lbH);
            this.panel1.Controls.Add(this.addInfo);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.btnSend);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.txtThongtin);
            this.panel1.Controls.Add(this.txtprice);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.txtSendInfo);
            this.panel1.Controls.Add(this.lb);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.lbdem);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.pictureb);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lstClientIP);
            this.panel1.Controls.Add(this.txtMessage);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtInfo);
            this.panel1.Location = new System.Drawing.Point(2, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1136, 656);
            this.panel1.TabIndex = 47;
            // 
            // txtPrice2
            // 
            this.txtPrice2.Location = new System.Drawing.Point(34, 244);
            this.txtPrice2.Name = "txtPrice2";
            this.txtPrice2.Size = new System.Drawing.Size(62, 27);
            this.txtPrice2.TabIndex = 73;
            this.txtPrice2.Text = "0";
            // 
            // SendPicture
            // 
            this.SendPicture.AutoSize = true;
            this.SendPicture.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SendPicture.Depth = 0;
            this.SendPicture.Icon = null;
            this.SendPicture.Location = new System.Drawing.Point(959, 324);
            this.SendPicture.MouseState = MaterialSkin.MouseState.HOVER;
            this.SendPicture.Name = "SendPicture";
            this.SendPicture.Primary = true;
            this.SendPicture.Size = new System.Drawing.Size(91, 36);
            this.SendPicture.TabIndex = 72;
            this.SendPicture.Text = "GỬI ẢNH";
            this.SendPicture.UseVisualStyleBackColor = true;
            this.SendPicture.Click += new System.EventHandler(this.SendPicture_Click);
            // 
            // addPicture
            // 
            this.addPicture.AutoSize = true;
            this.addPicture.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.addPicture.Depth = 0;
            this.addPicture.Icon = null;
            this.addPicture.Location = new System.Drawing.Point(793, 324);
            this.addPicture.MouseState = MaterialSkin.MouseState.HOVER;
            this.addPicture.Name = "addPicture";
            this.addPicture.Primary = true;
            this.addPicture.Size = new System.Drawing.Size(109, 36);
            this.addPicture.TabIndex = 71;
            this.addPicture.Text = "THÊM ẢNH";
            this.addPicture.UseVisualStyleBackColor = true;
            this.addPicture.Click += new System.EventHandler(this.addPicture_Click);
            // 
            // lbM
            // 
            this.lbM.AutoSize = true;
            this.lbM.BackColor = System.Drawing.Color.Transparent;
            this.lbM.Font = new System.Drawing.Font("Times New Roman", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbM.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lbM.Location = new System.Drawing.Point(608, 0);
            this.lbM.Name = "lbM";
            this.lbM.Size = new System.Drawing.Size(51, 37);
            this.lbM.TabIndex = 56;
            this.lbM.Text = "00";
            // 
            // lbH
            // 
            this.lbH.AutoSize = true;
            this.lbH.BackColor = System.Drawing.Color.Transparent;
            this.lbH.Font = new System.Drawing.Font("Times New Roman", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbH.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbH.Location = new System.Drawing.Point(517, 0);
            this.lbH.Name = "lbH";
            this.lbH.Size = new System.Drawing.Size(34, 37);
            this.lbH.TabIndex = 55;
            this.lbH.Text = "1";
            // 
            // addInfo
            // 
            this.addInfo.AutoSize = true;
            this.addInfo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.addInfo.Depth = 0;
            this.addInfo.Icon = null;
            this.addInfo.Location = new System.Drawing.Point(1020, 580);
            this.addInfo.MouseState = MaterialSkin.MouseState.HOVER;
            this.addInfo.Name = "addInfo";
            this.addInfo.Primary = true;
            this.addInfo.Size = new System.Drawing.Size(106, 36);
            this.addInfo.TabIndex = 70;
            this.addInfo.Text = "CẬP NHẬT";
            this.addInfo.UseVisualStyleBackColor = true;
            this.addInfo.Click += new System.EventHandler(this.addInfo_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(574, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 37);
            this.label11.TabIndex = 57;
            this.label11.Text = ":";
            // 
            // btnSend
            // 
            this.btnSend.AutoSize = true;
            this.btnSend.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSend.Depth = 0;
            this.btnSend.Icon = null;
            this.btnSend.Location = new System.Drawing.Point(396, 558);
            this.btnSend.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSend.Name = "btnSend";
            this.btnSend.Primary = true;
            this.btnSend.Size = new System.Drawing.Size(66, 36);
            this.btnSend.TabIndex = 69;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnStart
            // 
            this.btnStart.AutoSize = true;
            this.btnStart.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnStart.Depth = 0;
            this.btnStart.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnStart.ForeColor = System.Drawing.Color.Yellow;
            this.btnStart.Icon = null;
            this.btnStart.Location = new System.Drawing.Point(490, 558);
            this.btnStart.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnStart.Name = "btnStart";
            this.btnStart.Primary = true;
            this.btnStart.Size = new System.Drawing.Size(75, 36);
            this.btnStart.TabIndex = 68;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // txtThongtin
            // 
            this.txtThongtin.BackColor = System.Drawing.Color.White;
            this.txtThongtin.Location = new System.Drawing.Point(778, 407);
            this.txtThongtin.Multiline = true;
            this.txtThongtin.Name = "txtThongtin";
            this.txtThongtin.Size = new System.Drawing.Size(348, 142);
            this.txtThongtin.TabIndex = 67;
            // 
            // txtprice
            // 
            this.txtprice.Location = new System.Drawing.Point(128, 49);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(71, 27);
            this.txtprice.TabIndex = 66;
            this.txtprice.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label14.ForeColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(778, 384);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(168, 22);
            this.label14.TabIndex = 65;
            this.label14.Text = "Thông tin sản phẩm:";
            // 
            // txtSendInfo
            // 
            this.txtSendInfo.Location = new System.Drawing.Point(778, 589);
            this.txtSendInfo.Name = "txtSendInfo";
            this.txtSendInfo.Size = new System.Drawing.Size(236, 27);
            this.txtSendInfo.TabIndex = 64;
            // 
            // lb
            // 
            this.lb.AutoSize = true;
            this.lb.BackColor = System.Drawing.Color.Transparent;
            this.lb.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lb.ForeColor = System.Drawing.Color.White;
            this.lb.Location = new System.Drawing.Point(778, 564);
            this.lb.Name = "lb";
            this.lb.Size = new System.Drawing.Size(208, 22);
            this.lb.TabIndex = 63;
            this.lb.Text = "Nhập thông tin sản phẩm:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(13, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 26);
            this.label7.TabIndex = 62;
            this.label7.Text = "Lượt đấu giá";
            // 
            // lbdem
            // 
            this.lbdem.AutoSize = true;
            this.lbdem.BackColor = System.Drawing.Color.Transparent;
            this.lbdem.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbdem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbdem.Location = new System.Drawing.Point(147, 95);
            this.lbdem.Name = "lbdem";
            this.lbdem.Size = new System.Drawing.Size(24, 25);
            this.lbdem.TabIndex = 61;
            this.lbdem.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(13, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 26);
            this.label4.TabIndex = 59;
            this.label4.Text = "Giá hiện tại";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(205, 51);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 25);
            this.label10.TabIndex = 58;
            this.label10.Text = "VNĐ";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // pictureb
            // 
            this.pictureb.BackColor = System.Drawing.Color.Transparent;
            this.pictureb.Location = new System.Drawing.Point(715, 10);
            this.pictureb.Name = "pictureb";
            this.pictureb.Size = new System.Drawing.Size(376, 308);
            this.pictureb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureb.TabIndex = 54;
            this.pictureb.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(400, 274);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 22);
            this.label3.TabIndex = 53;
            this.label3.Text = "Client IP:";
            // 
            // lstClientIP
            // 
            this.lstClientIP.FormattingEnabled = true;
            this.lstClientIP.ItemHeight = 20;
            this.lstClientIP.Location = new System.Drawing.Point(400, 297);
            this.lstClientIP.Name = "lstClientIP";
            this.lstClientIP.Size = new System.Drawing.Size(165, 204);
            this.lstClientIP.TabIndex = 52;
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(13, 558);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(367, 27);
            this.txtMessage.TabIndex = 51;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(13, 515);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 28);
            this.label2.TabIndex = 50;
            this.label2.Text = "Message:";
            // 
            // txtInfo
            // 
            this.txtInfo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtInfo.Location = new System.Drawing.Point(17, 277);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ReadOnly = true;
            this.txtInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtInfo.Size = new System.Drawing.Size(367, 224);
            this.txtInfo.TabIndex = 48;
            // 
            // txtIP
            // 
            this.txtIP.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtIP.Location = new System.Drawing.Point(1025, 30);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(97, 27);
            this.txtIP.TabIndex = 49;
            this.txtIP.Text = "127.0.0.1:9000";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(952, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 20);
            this.label1.TabIndex = 47;
            this.label1.Text = "Server";
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1139, 713);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtIP);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "TCP/IP Server";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureb)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtPrice2;
        private MaterialSkin.Controls.MaterialRaisedButton SendPicture;
        private MaterialSkin.Controls.MaterialRaisedButton addPicture;
        private MaterialSkin.Controls.MaterialRaisedButton addInfo;
        private MaterialSkin.Controls.MaterialRaisedButton btnSend;
        private MaterialSkin.Controls.MaterialRaisedButton btnStart;
        private System.Windows.Forms.TextBox txtThongtin;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtSendInfo;
        private System.Windows.Forms.Label lb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbdem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbM;
        private System.Windows.Forms.Label lbH;
        private System.Windows.Forms.PictureBox pictureb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lstClientIP;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer2;
    }
}

