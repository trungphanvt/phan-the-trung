using SimpleTcp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Socil
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SimpleTcpClient client;

        private void Form1_Load(object sender, EventArgs e)
        {
            client = new(txtIP.Text);
            client.Events.Connected += Events_ClientConnected;
            client.Events.DataReceived += Events_DataReceived;
            client.Events.Disconnected += Events_ClientDisconnected;
        }

        private void Events_ClientDisconnected(object sender, ClientDisconnectedEventArgs e)
        {

        }

        private void Events_DataReceived(object sender, DataReceivedEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                richTextBox1.Text += Encoding.UTF8.GetString(e.Data);
            });

        }

        private void Events_ClientConnected(object sender, ClientConnectedEventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                client.Connect();
                button1.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "messege", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSebd_Click(object sender, EventArgs e)
        {
            client.Send(txtMessege.Text);
        }
    }
}